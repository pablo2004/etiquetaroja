// INIT CARUSE

var promocionCliente = $('#promocionCliente');

var initPorCliente = function(cliente_id){

    ajax("POST", URL+"promociones", {"cliente_id": cliente_id}, 
    function(){
        app.preloader.show();
    }, 
    function(data){
    
        app.preloader.hide();
    
        if(data['status'] == 1){

            promocionCliente.find('a').remove();
            var template = '';
            
            template += '<a data-name="promocion" data-params="{params}" class="go-link" href="/">';
            template += '<div class="card demo-card-header-pic">';
            template += '<div style="background-image:url({imagen})" class="card-header align-items-flex-end title-shadow">';
            template += '  {nombre}';
            template += '</div>';
            template += '<div class="card-content card-content-padding">';
            template += '  <p>{descripcion}</p>';
            template += '</div>';
            template += '</div>';
            template += '</a>';

            var ads = data.ads;
            var ad = null;
            var vars = {};

            for(var i in ads){
                ad = ads[i];
                vars = {
                    "params": objectToBase64(ad),
                    "imagen": ad['imagen_promocion'],
                    "nombre": ad['nombre'],
                    "descripcion": ad['descripcion']
                };
                promocionCliente.append(parseTemplate(template, vars));
            }

        }
    
    }, function(e){
        app.preloader.hide();
        app.toast.show({ text: e, closeButton: true });
    });    

}

var cliente = Storage.getParam("cliente");
initPorCliente(cliente.cid);