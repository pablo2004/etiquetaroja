
var favoritosListado = $('#favoritosListado');

var initFavoritos = function(id, token){

    ajax("GET", URL+"favoritos/"+id, {}, 
    function(){
        app.preloader.show();
    }, 
    function(data){
    
        app.preloader.hide();
    
        if(data['status'] == 1){

            favoritosListado.html("");
            favoritosListado.find("li").remove();
            
            var template = '';

            template += '<li data-name="promocion" data-params="{params}" class="go-link">';
            template += '<a href="#" class="item-link item-content">';
            template += '<div class="item-media">';
            template += '<img src="{imagen}" width="44"/>';
            template += '</div>';
            template += '<div class="item-inner">';
            template += '<div class="item-title-row"><div class="item-title">{nombre}</div></div>';
            template += '<div class="item-subtitle">{descripcion}</div>';
            template += '</div>';
            template += '</div>';
            template += '</a>';
            template += '</li>';

            var ads = data.ads;
            var ad = null;
            var vars = {};

            for(var i in ads){
                ad = ads[i];
                vars = {
                    "params": objectToBase64(ad),
                    "imagen": ad['imagen_promocion'],
                    "nombre": ad['nombre'],
                    "descripcion": ad['descripcion']
                };
                favoritosListado.append(parseTemplate(template, vars));
            }
    
        }
    
    }, function(e){
        app.preloader.hide();
        app.toast.show({ text: e, closeButton: true });
    },
    { "Token": token });    

}


initFavoritos(Storage.getUser().id, Storage.getUser().oid);