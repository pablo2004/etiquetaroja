// INIT CARUSE
var notificacionesListado = $('#notificacionesListado');

var initNotificaciones = function(id, token){

    ajax("GET", URL+"notificaciones/"+id, {}, 
    function(){
        app.preloader.show();
    }, 
    function(data){
    
        app.preloader.hide();
    
        if(data['status'] == 1){

            notificacionesListado.html("");
            notificacionesListado.find("li").remove();
            
            var template = '';

            template += '<li data-name="notificacion" data-params="{params}" class="go-link">';
            template += '<a href="#" class="item-link item-content">';
            template += '<div class="item-media">';
            template += '<img src="{imagen}" width="44"/>';
            template += '</div>';
            template += '<div class="item-inner">';
            template += '<div class="item-title-row"><div class="item-title">{nombre}</div></div>';
            template += '<div class="item-subtitle">{descripcion}</div>';
            template += '</div>';
            template += '</div>';
            template += '</a>';
            template += '</li>';

            var notifications = data.notifications;
            var notification = null;
            var vars = {};

            for(var i in notifications){
                notification = notifications[i];
                vars = {
                    "params": objectToBase64(notification),
                    "imagen": notification['imagen'],
                    "nombre": notification['nombre'],
                    "descripcion": notification['descripcion']
                };
                notificacionesListado.append(parseTemplate(template, vars));
            }
    
        }
    
    }, function(e){
        app.preloader.hide();
        app.toast.show({ text: e, closeButton: true });
    },
    { "Token": token });    

}


initNotificaciones(Storage.getUser().id, Storage.getUser().oid);