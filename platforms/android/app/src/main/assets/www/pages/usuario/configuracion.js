var user = Storage.getUser();

if(user !== null){

    setSelectData($('#fecha_mes'), dateMonths);
    setSelectData($('#fecha_dia'), dateDays);
    setSelectData($('#fecha_ano'), dateYears);

    if(user.gps_activo){
        $('#gps_activo_checkbox').attr("checked", true);
    }

    if(user.notificacion_activa){
        $('#notificacion_activa_checkbox').attr("checked", true);
    }

    if(user.fecha_nacimiento != null){
        var fechaNacimiento = user.fecha_nacimiento;
        var parts = fechaNacimiento.split("-");
        if(parts != null && parts.length == 3){
            user["fecha_ano"] = parseInt(parts[0]);
            user["fecha_mes"] = parseInt(parts[1]);
            user["fecha_dia"] = parseInt(parts[2]);
        }
    }

    app.form.fillFromData('#editar-usuario-form', user);  

    $('#editar-button').on("click", function(){
        var dataForm = app.form.convertToData('#editar-usuario-form');
        initUsuarioEditar(user.id, dataForm);
    });

}


var gpsActivo = app.toggle.create({
    el: '#gps_activo',
    on: {
        change: function () {

            gps_activo = (gpsActivo.checked) ? 1 : 0;

            if(gps_activo == 0){

                app.dialog.confirm("Confirmar", "Es recomendado el uso de gps para obtener las mejores promociones.", function(){
                    initUsuarioEditaCampo(user.id, "gps_activo", gps_activo);
                }, function(){
                    $('#gps_activo_checkbox').trigger("click");
                });

            }
            else {
                initUsuarioEditaCampo(user.id, "gps_activo", gps_activo);
            }

        }
    } 
});

var notificacionActiva = app.toggle.create({
    el: '#notificacion_activa',
    on: {
        change: function () {
            notificacion_activa = (notificacionActiva.checked) ? 1 : 0;
            initUsuarioEditaCampo(user.id, "notificacion_activa", notificacion_activa);
        }
    } 
});

var initUsuarioEditaCampo = function(id, campo, valor){

    id = parseInt(id);
    data = {"valor": valor};

    ajax("POST", URL+"usuarioGuarda/"+id+"/"+campo, data, 
    function(){
        app.preloader.show();
    }, 
    function(data){
    
        app.preloader.hide();
    
        if(data['status'] == 0){
            app.toast.show({ text: data['message'], closeButton: true });
        }
        else {
            Storage.setUser(data['user']);
        }
    
    }, function(e){
        app.preloader.hide();
        app.toast.show({ text: e, closeButton: true });
    }, { "Token": Storage.getUser().oid });    

}

var initUsuarioEditar = function(id, data){

    ajax("POST", URL+"usuarioEditar/"+id, data, 
    function(){
        app.preloader.show();
    }, 
    function(data){
    
        app.preloader.hide();
    
        if(data['status'] == 1){
            Storage.setUser(data["user"]);
            app.toast.show({ text: "Información guardada.", closeButton: true });
        }
        else{
            app.toast.show({ text: data['message'], closeButton: true });
        }
    
    }, function(e){
        app.preloader.hide();
        app.toast.show({ text: e, closeButton: true });
    }, { "Token": Storage.getUser().oid });    

}