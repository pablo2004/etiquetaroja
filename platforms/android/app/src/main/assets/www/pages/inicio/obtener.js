var obtener = Storage.getParam("obtener");

var initSucursal = function(id, lat, lng){
 
    ajax("POST", URL+"promociones_sucursales/"+id, {"lat": lat, "lng": lng}, 
    function(){
        app.preloader.show();
    }, 
    function(data){
    
        app.preloader.hide();
    
        if(data['status'] == 1){
    
            var template = "";

            template += '<li>';
            template += '<label class="item-radio item-content">';

            template += '<input type="radio" class="radio-sucursal" name="demo-radio" value="{id}" data-whatsapp="{whatsapp}" />';
            template += '<i class="icon icon-radio "></i>';

            template += '<div class="item-inner">';
            template += '<div class="item-title">{nombre}</div>';
            template += '<div class="item-text">{direccion}</div>';
            template += '</div>';

            template += '<div data-url="https://www.google.com/maps/search/?api=1&query={coordenadas}" style="margin-right:10px;" class="ir-sucursal item-icon"><i class="fas fa-map-marker-alt fa-2x et-color"></i><br /><small>{distancia}k</small></div>';

            template += '</label>';
            template += '</li>';

            var sucursales = data["places"];
            var sucursal = null;
            var sucursalContainer = $('#sucursales');

            sucursalContainer.find('li').remove();

            for(var i in sucursales){
                sucursal = sucursales[i];

                vars = {
                    "id": sucursal["sid"],
                    "imagen": sucursal["imagen_cliente"],
                    "whatsapp": sucursal["whatsapp"],
                    "nombre":  sucursal["nombre"],
                    "direccion": sucursal["direccion"],
                    "coordenadas": sucursal["coordenadas"],
                    "distancia": sucursal["distance"].toFixed(0)
                };

                sucursalContainer.append(parseTemplate(template, vars));
            }

            $('.radio-sucursal').on("click", function(){

                var self = $(this);
                sucursal_id = self.val();

                if(Storage.getUser() !== null && obtener !== null){
                    getPromocion(obtener['id'], sucursal_id, 1);
                }
                else {
                    mainView.router.navigate({ name: "login" });
                }

            });

            $('.ir-sucursal').on("click", function(){
                var self = $(this);
                var url = self.data("url");
                window.location.href = url;

            });

        }
    
    }, function(e){
        app.preloader.hide();
        app.toast.show({ text: e, closeButton: true });
    });    

}

var getPromocion = function(id, sucursal_id, actualiza){

    ajax("POST", URL+"obtenerPromocion/"+Storage.getUser().id, {
        "promocion_id": id, 
        "sucursal_id": sucursal_id,
        "actualiza": actualiza
    }, 
    function(){
        app.preloader.show();
    }, 
    function(data){
    
        app.preloader.hide();
    
        if(data['status'] == 1){
    
            // promo_qr, promo_whatsapp, promo_texto
            promo = data["promo"];
            sucursal = data["sucursal"]

            var areaScan = $('#areaScan');
            var promocionReportar = $('#promocionReportar');
            var promocionQr = $('#promocionQr');
            var promocionBarcode = $('#promocionBarcode');
            var promocionTexto = $('#promocionTexto');

            var verCodigoQR = $('#verCodigoQR');
            var verCodigoBC = $('#verCodigoBC');

            if(promocion.promo_qr && promocion.promo_barcode){

                areaScan.show();

                codigoQr = (promocion.promo_texto.length === 0) ? promo["codigo"] : promocion.promo_texto;
                
                barcode = 'https://jolijun.com/barcode/html/image.php?filetype=PNG&dpi=72&thickness=30&scale=2&rotation=0&font_family=Arial.ttf&font_size=8&setChecksum=&code=BCGcode39&text='+codigoQr;
                qrcode = 'https://api.qrserver.com/v1/create-qr-code/?size=150x150&data='+codigoQr;
    
              
                promocionQr.attr("src", qrcode);
                promocionBarcode.attr("src", barcode);

                promocionQr.hide();
                promocionBarcode.hide();

                verCodigoQR.on("click", function(e){
                    verCodigoQR.addClass("button-active");
                    verCodigoBC.removeClass("button-active");
                    promocionQr.show();
                    promocionBarcode.hide();
                });

                verCodigoBC.on("click", function(e){
                    verCodigoQR.removeClass("button-active");
                    verCodigoBC.addClass("button-active");
                    promocionQr.hide();
                    promocionBarcode.show();
                });

                verCodigoQR.trigger("click");
            
            }
            else{

                if(!promocion.promo_qr && !promocion.promo_barcode){
                    areaScan.hide();
                }
                else {

                    areaScan.show();
                    areaScan.find('.segmented').hide();

                    codigoQr = (promocion.promo_texto.length === 0) ? promo["codigo"] : promocion.promo_texto;
                
                    barcode = 'https://jolijun.com/barcode/html/image.php?filetype=PNG&dpi=72&thickness=30&scale=2&rotation=0&font_family=Arial.ttf&font_size=8&setChecksum=&code=BCGcode39&text='+codigoQr;
                    qrcode = 'https://api.qrserver.com/v1/create-qr-code/?size=150x150&data='+codigoQr;
        
                 
                    promocionBarcode.attr("src", barcode);

                    if(promocion.promo_qr){
                        promocionQr.attr("src", qrcode);
                        promocionQr.show();
                        promocionBarcode.hide();
                    }
                    else {
                        promocionBarcode.attr("src", qrcode);
                        promocionBarcode.show();
                        promocionQr.hide();
                    }

                }

            }

            if(promocion.promo_texto.length !== 0){
                promocionTexto.show();
                promocionTexto.find('.codigo-promocinal').html(promocion.promo_texto);
            }
            else{
                promocionTexto.hide();
            }

            
            var promocionWhatsapp = $('#promocionWhatsapp');
            var contactoWhatsapp = $('#contactoWhatsapp');

            if(promocion.promo_whatsapp && sucursal){
     
                sucursalNombre = sucursal["nombre"];
                sucursalWhatsapp = sucursal["whatsapp"];
                activarWhatsapp = true;

                if(typeof sucursalNombre == 'undefined'){
                    sucursalNombre = "No disponible";
                    sucursalWhatsapp = "";
                    activarWhatsapp = false;
                }

                promocionWhatsapp.show();
                contactoWhatsapp.html("Ponte en contacto con "+sucursalNombre+" para hacer valida tu promoci&oacute;n.");

                whatsapp = promocionWhatsapp.find('.share-whatsapp');

                promocionWhatsapp.on("click", function(e){
                    e.preventDefault();
        
                    if(activarWhatsapp){
                        w_texto = promocion.promo_whatsapp_texto;
                        var shareText = (w_texto !== null && w_texto.length > 0) ? w_texto : promocion.nombre;
                        window.location.href = "https://wa.me/"+sucursalWhatsapp+"?text="+shareText+" - "+APP_URL+"canjes/canjear/?codigo="+promo.codigo;
                    }
                    else {
                        app.dialog.alert("Whatsapp no disponible.", "Mensaje");
                    }

                });

            }
            else{
                promocionWhatsapp.hide();
            }
            

            promocionReportar.on("click", function(){

                var mensaje = '';

                mensaje += '<img style="width:100%;" src="img/er_escucha.png" />';
                mensaje += '<div style="text-align:center;font-size:10px;color:#A4A4A4;">envianos tus comentarios<br />pronto nos estaremos poniento en contacto contigo</div>';

                app.dialog.prompt("", mensaje, function (mensaje) {
                    reportar(promocion.id, mensaje);
                });

            });

        }
    
    }, function(e){
        app.preloader.hide();
        app.toast.show({ text: e, closeButton: true });
    }, { "Token": Storage.getUser().oid });    

}

var reportar = function(id, mensaje){

    ajax("POST", URL+"reportar/"+Storage.getUser().id, {"promocion_id": id, "mensaje": mensaje}, 
    function(){
        app.preloader.show();
    }, 
    function(data){
    
        app.preloader.hide();
    
        if(data['status'] == 1){
            app.toast.show({ text: "¡Gracias por tu mensaje!", closeButton: true });
        }
    
    }, function(e){
        app.preloader.hide();
        app.toast.show({ text: e, closeButton: true });
    }, { "Token": Storage.getUser().oid });    

}

if(obtener !== null){

    initSucursal(promocion.id, Storage.getLat(), Storage.getLng());
    getPromocion(promocion.id, 0, 0);

}
else {
    mainView.router.back(url, {});
}