
var initRecovery = function(data){

    ajax("POST", URL+"contrasenaRecuperar/", data, 
    function(){
        app.preloader.show();
    }, 
    function(data){
    
        app.preloader.hide();
    
        if(data['status'] == 1){
    
            app.dialog.alert("¡Se ha enviado un correo a tu cuenta, por favor sigue los pasos para recuperar tu contraseña!", "Exito", function(){
                mainView.router.navigate({ name: "inicio" });
            })
         
        }
        else{
            app.toast.show({ text: data['message'], closeButton: true });
        }
    
    }, function(e){
        app.preloader.hide();
        app.toast.show({ text: e, closeButton: true });
    });    

}

$('#recuperar-button').on("click", function(e){

    e.preventDefault();
    var dataForm = app.form.convertToData('#recuperar-form');
    initRecovery(dataForm);

});
