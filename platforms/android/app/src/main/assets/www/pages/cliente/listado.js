// INIT CARUSE
var clientesListado = $('#clientesListado');

var initClientes = function(busqueda){

    ajax("POST", URL+"clientes", {"busqueda": busqueda}, 
    function(){
        app.preloader.show();
    }, 
    function(data){
    
        app.preloader.hide();
    
        if(data['status'] == 1){

            clientesListado.html("");
            clientesListado.find("a").remove();
            
            var template = '';

            template += '<a data-name="cliente" data-params="{params}" class="go-link" href="/">';
            template += '<div class="card demo-card-header-pic">';
            template += '<div style="background-image:url({imagen})" class="card-header align-items-flex-end title-shadow">';
            template += '  {nombre}';
            template += '</div>';
            template += '</div>';
            template += '</a>';

            var clients = data.clients;
            var client = null;
            var vars = {};

            for(var i in clients){
                client = clients[i];
                vars = {
                    "params": objectToBase64(client),
                    "imagen": client['imagen_cliente'],
                    "nombre": client['cliente']
                };
                clientesListado.append(parseTemplate(template, vars));
            }
    
        }
    
    }, function(e){
        app.preloader.hide();
        app.toast.show({ text: e, closeButton: true });
    });    

}


initClientes();