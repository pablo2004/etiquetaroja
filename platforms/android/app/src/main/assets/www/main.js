var URL = "https://etiquetaroja.minubeonline.com/api/";
var APP_URL = "https://etiquetaroja.minubeonline.com/";
var app = null;
var mainView = null;
var FACEBOOK_PERMISSION = ["public_profile", "email"];

var Storage = {

  scanning: false, 

  params: {},

  lat: 0,

  lng: 0,

  db: null,

  user: null,

  push: null,

  loginGoogle: null,

  loginFacebook: null,

  isScanning: function(){
    return this.scanning;
  },

  setScanning: function(isScanning){
    this.scanning = isScanning;
  },

  addParam: function(index, value){
    this.params[index] = value;
  },

  getParam: function(index){
    return this.params[index];
  },

  getDb: function(){
    return this.db;
  },

  setDb: function(db){
    this.db = db;
  },

  getUser: function(){
    return this.user;
  },

  setUser: function(user){
    this.user = user;
  },

  getPush: function(){
    return this.push;
  },

  setPush: function(push){
    this.push = push;
  },

  setLoginGoogle: function(loginGoogle){
    this.loginGoogle = loginGoogle;
  },

  getLoginGoogle: function(){
    return this.loginGoogle;
  },

  setLoginFacebook: function(loginFacebook){
    this.loginFacebook = loginFacebook;
  },

  getLoginFacebook: function(){
    return this.loginFacebook;
  },

  setLat: function(lat){
    this.lat = lat;
  },

  getLat: function(){
    return this.lat;
  },

  setLng: function(lng){
    this.lng = lng;
  },

  getLng: function(){
    return this.lng;
  }

};

Storage.setDb(window.localStorage);

if (typeof cordova !== 'undefined') {

  document.addEventListener("deviceready", function () { 

    var showPushGCM = function(payload){

      var push = {
        imagen: payload.imagen,
        nombre: payload.nombre,
        mensaje: payload.mensaje,
        ruta: payload.ruta,
        params: payload.params
      };

      Storage.setPush(push);
      mainView.router.navigate({ name: "push" });

    }

    /*
    cordova.plugins.firebase.messaging.onMessage(function(payload) {
      showPushGCM(payload);
    });

    cordova.plugins.firebase.messaging.onBackgroundMessage(function(payload) {
      
      setTimeout(function(){
        showPushGCM(payload);
      }, 1000);
      
    });
    */

    /*window.plugins.getKeyHashes(function(keyHashes){
      logObject("keyhash", "facebook", keyHashes);
    }, function(){}); */

    initApp(); 
    
  }, false);
  document.addEventListener("backbutton", function() {

    if(window.cordova.platformId === 'android'){

      var prev = mainView.router.previousRoute;
      var current = mainView.router.currentRoute;
      var panelCurrent = app.panel.get('.panel-left');

      if(typeof prev.url !== 'undefined'){

        if(current.url == '/inicio/index/'){

          if(panelCurrent.opened){
            panelCurrent.close();
          }
          else{

            app.dialog.confirm('¿Deseas Salir de la aplicación?', function () {
              navigator.app.exitApp();
            });

          }
          
        }
        else{

          if(Storage.isScanning()){ 
            escanearQrOff(null);
          }
          else {
            mainView.router.back(prev.url, {});
          }
          
        }

      }
      else{

        if(panelCurrent.opened){
          panelCurrent.close();
        }
        else{
          app.dialog.confirm('¿Deseas Salir de la aplicación?', function () {
            navigator.app.exitApp();
          });
          
        }

      }
      
    }

  }, false);

}
else {

  $(document).ready(function () { initApp(); });
  window.onpopstate = function(e){ e.preventDefault(); };

}

var initApp = function () {

  app = new Framework7({
    root: '#app',
    name: 'Etiquet Roja',
    id: 'mx.etiquetaroja.app',
    routes: [
      {
        xhrCache: false,
        name: 'mapa',
        path: '/inicio/mapa/',
        url: 'pages/inicio/mapa.html',
        on: {
          pageInit: function (e, page) {
            $.getScript('pages/inicio/mapa.js', function(){ });
          }
        }
      },      
      {
        xhrCache: false,
        name: "inicio",
        path: '/inicio/index/',
        url: 'pages/inicio/index.html',
        on: {
          pageInit: function (e, page) {
            $.getScript('pages/inicio/index.js', function(){ });
          }
        }
      },
      {
        xhrCache: false,
        name: "promocion",
        path: '/inicio/promocion/',
        url: 'pages/inicio/promocion.html',
        on: {
          pageInit: function (e, page) {
            $.getScript('pages/inicio/promocion.js', function(){ });
          }
        }
      },
      {
        xhrCache: false,
        name: "obtener",
        path: '/inicio/obtener/',
        url: 'pages/inicio/obtener.html',
        on: {
          pageInit: function (e, page) {
            $.getScript('pages/inicio/obtener.js', function(){ });
          }
        }
      },
      {
        xhrCache: false,
        name: "sucursal",
        path: '/sucursal/detalle/',
        url: 'pages/sucursal/detalle.html',
        on: {
          pageInit: function (e, page) {
            $.getScript('pages/sucursal/detalle.js', function(){ });
          }
        }
      },
      {
        xhrCache: false,
        name: "login",
        path: '/usuario/login/',
        url: 'pages/usuario/login.html',
        on: {
          pageInit: function (e, page) {

            $.getScript('https://apis.google.com/js/platform.js', function(){
              $.getScript('pages/usuario/login.js', function(){ });
            });
           
          }
        }
      },
      {
        xhrCache: false,
        name: "registro",
        path: '/usuario/registro/',
        url: 'pages/usuario/registro.html',
        on: {
          pageInit: function (e, page) {
            $.getScript('pages/usuario/registro.js', function(){ });
          }
        }
      },
      {
        xhrCache: false,
        name: "configuracion",
        path: '/usuario/configuracion/',
        url: 'pages/usuario/configuracion.html',
        on: {
          pageInit: function (e, page) {
            $.getScript('pages/usuario/configuracion.js', function(){ });
          }
        }
      },
      {
        xhrCache: false,
        name: "clientes",
        path: '/cliente/listado/',
        url: 'pages/cliente/listado.html',
        on: {
          pageInit: function (e, page) {
            $.getScript('pages/cliente/listado.js', function(){ });
          }
        }
      },
      {
        xhrCache: false,
        name: "cliente",
        path: '/cliente/detalle/',
        url: 'pages/cliente/detalle.html',
        on: {
          pageInit: function (e, page) {
            $.getScript('pages/cliente/detalle.js', function(){ });
          }
        }
      },
      {
        xhrCache: false,
        name: "categoria",
        path: '/categoria/detalle/',
        url: 'pages/categoria/detalle.html',
        on: {
          pageInit: function (e, page) {
            $.getScript('pages/categoria/detalle.js', function(){ });
          }
        }
      },
      {
        xhrCache: false,
        name: "favorito",
        path: '/usuario/favorito/',
        url: 'pages/usuario/favorito.html',
        on: {
          pageInit: function (e, page) {
            $.getScript('pages/usuario/favorito.js', function(){ });
          }
        }
      },
      {
        xhrCache: false,
        name: "notificaciones",
        path: '/notificacion/listado/',
        url: 'pages/notificacion/listado.html',
        on: {
          pageInit: function (e, page) {
            $.getScript('pages/notificacion/listado.js', function(){ });
          }
        }
      },
      {
        xhrCache: false,
        name: "notificacion",
        path: '/notificacion/detalle/',
        url: 'pages/notificacion/detalle.html',
        on: {
          pageInit: function (e, page) {
            $.getScript('pages/notificacion/detalle.js', function(){ });
          }
        }
      },
      {
        xhrCache: false,
        name: "push",
        path: '/notificacion/push/',
        url: 'pages/notificacion/push.html',
        on: {
          pageInit: function (e, page) {
            $.getScript('pages/notificacion/push.js', function(){ });
          }
        }
      },
      {
        xhrCache: false,
        name: "escaner",
        path: '/escaner/escaner/',
        url: 'pages/escaner/escaner.html',
        on: {
          pageInit: function (e, page) {
            $.getScript('pages/escaner/escaner.js', function(){ });
          }
        }
      },
      {
        xhrCache: false,
        name: "tarjeta",
        path: '/usuario/tarjeta/',
        url: 'pages/usuario/tarjeta.html',
        on: {
          pageInit: function (e, page) {
            $.getScript('pages/usuario/tarjeta.js', function(){ });
          }
        }
      },
      {
        xhrCache: false,
        name: "tarjetadetalle",
        path: '/usuario/tarjetadetalle/',
        url: 'pages/usuario/tarjetadetalle.html',
        on: {
          pageInit: function (e, page) {
            $.getScript('pages/usuario/tarjetadetalle.js', function(){ });
          }
        }
      },
      {
        xhrCache: false,
        name: "recuperar",
        path: '/usuario/recuperar/',
        url: 'pages/usuario/recuperar.html',
        on: {
          pageInit: function (e, page) {

            $.getScript('pages/usuario/recuperar.js', function(){ });
            
          }
        }
      },
      {
        xhrCache: false,
        name: "terminos",
        path: '/informacion/terminos/',
        url: 'pages/informacion/terminos.html'
      },
      {
        xhrCache: false,
        name: "aviso",
        path: '/informacion/aviso/',
        url: 'pages/informacion/aviso.html'
      }

    ]
  });

  mainView = app.views.create('.view-main');
  mainView.router.navigate({ name: "inicio" });

  $(document).on("click", ".go-link", function(e){

    e.preventDefault();
    var self = $(this);

    var sidebar = self.data("sidebar");
    var params = self.data("params");
    var login = self.data("login");

    if(typeof params !== 'undefined'){
      data = base64ToObject(params);
      Storage.addParam(self.data("name"), data);
    }

    if(typeof sidebar !== 'undefined'){
      app.panel.get('.panel-left').close();
    }
    
    if(login){

      if(Storage.getUser() === null){
        mainView.router.navigate({
          name: "login",
          ignoreCache: true
        });
      }
      else {
        mainView.router.navigate({
          name: self.data("name"),
          ignoreCache: true
        });
      }

    }
    else {
      mainView.router.navigate({
        name: self.data("name"),
        ignoreCache: true
      });
    }

  });

  $(document).on('click', '.go-back', function(e){
    e.preventDefault();
    var self = $(this);
    var url = "/";
    if(self.data('url')){
      url = self.data('url');
    }
    mainView.router.back(url, {});
  });
  
  getPos();

}

var getPos = function(){

  navigator.geolocation.getCurrentPosition(function (position) {

    var lat = position.coords.latitude.toFixed(7);
    var lng = position.coords.longitude.toFixed(7);

    Storage.setLat(lat);
    Storage.setLng(lng);

    setTimeout(function(){
      getPos();
    }, 30000);

  }, function (error) {});

};

window.onerror = function (msg, url, line) {
  var idx = url.lastIndexOf("/");
  if (idx > -1) { url = url.substring(idx + 1); }
  var msg = "ERROR in " + url + " (line #" + line + "): " + msg;
  alert(msg);
  logObject("error", "msg", { "error": msg });
  return false; 
};
