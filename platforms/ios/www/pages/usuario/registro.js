var terminos = $('#terminos');

var dialogo = app.dialog.create({
    title: 'Confirmar',
    text: 'Para completar tu registro, debes aceptar los <b>Términos y Condiciones de Uso</b> y las <b>Políticas de Privacidad.</b>',
    buttons: [
      {
        text: 'Acepto',
        onClick: function(dialog, e){
            dialog.close();
            terminos.attr("checked", true);
            $('#register-button').trigger("click");
        }
      },
      {
        text: 'Cancelar',
        onClick: function(dialog, e){
            dialog.close();
        }
      }
    ]
});

var parseGoogle = function(data){ 

    if(data !== null){

        $('#passwordContainer').hide();

        $('#google_id').val(data['userId']);

        if(data['displayName']){
            var parts = data['displayName'].split(" ");
            if(parts.length == 2){
                $('#nombre').val(parts[0]);
                $('#apellido_paterno').val(parts[1]);
            }
        }

        if(data['email']){
            $('#email_registro').val(data['email']);
        }

    }

}

var parseFacebook = function(result){

    if(result !== null){

        $('#passwordContainer').hide();

        if(result['id']){
            $('#facebook_id').val(result['id']);
        }

        if(result['name']){
            var parts = result['name'].split(" ");
            if(parts.length == 2){
                $('#nombre').val(parts[0]);
                $('#apellido_paterno').val(parts[1]);
            }
        }

        if(result['email']){
            $('#email_registro').val(result['email']);
        }

        if(result['birthday']){
            var birthday = result['birthday'];
            birthday = birthday.split("/");

            if(birthday.length == 3){
                var day = parseInt(birthday[1]);
                var month = parseInt(birthday[0]);
                var year = birthday[2];

                $('#fecha_dia').val(day);
                $('#fecha_mes').val(month);
                $('#fecha_ano').val(year);
            }
            
        }

        if(result['gender']){

            genero_id = 0;

            if(result['gender'] == 'male'){
                genero_id = 1;
            }
            else {
                genero_id = 2;
            }

            $('#genero_box div[data-val='+genero_id+']').trigger("click");
            
        }

    }

}

var initRegistro = function(data){

    ajax("POST", URL+"usuarioRegistro/", data, 
    function(){
        app.preloader.show();
    }, 
    function(data){
    
        app.preloader.hide();
    
        if(data['status'] == 1){
    
            Storage.getDb().setItem("id", data["user"]["oid"]);
            Storage.setUser(data["user"]);
            mainView.router.navigate({ name: "inicio" });

        }
        else{
            app.toast.show({ text: data['message'], closeButton: true });
        }
    
    }, function(e){
        app.preloader.hide();
        app.toast.show({ text: e, closeButton: true });
    });    

}

$('#register-button').on("click", function(e){

    e.preventDefault();

    if(terminos.is(':checked')){
        var dataForm = app.form.convertToData('#registro-form');
        initRegistro(dataForm);
    }
    else {
        app.toast.show({ text: "Debes aceptar los términos y condiciones.", closeButton: true });
    }

});

$('#register-google').on('click', function(e){ 

    window.plugins.googleplus.login({
          'scopes': 'profile email'
        },
        function (data) {

            parseGoogle(data);
            dialogo.open();

        },
        function (msg) {
            app.toast.show({ text: "Fallo el acceso via Google.", closeButton: true });
        }
    );

});

$('#register-facebook').on('click', function(e){

    // facebookId+"/?fields=id,name,email,gender,birthday"
    // var permission = ["public_profile", "user_birthday", "email", "user_gender"];

    var permission = ["public_profile", "email"];

    facebookConnectPlugin.login(permission, function(data){

        if(data.status == 'connected'){

            var facebookId = data.authResponse.userID;
            facebookConnectPlugin.api(facebookId+"/?fields=id,name,email", permission, 
            function onSuccess (result) {
                parseFacebook(result);
                dialogo.open();

            }, function onError (error) {
                app.toast.show({ text: "Fallo el acceso via Facebook.", closeButton: true });
            });

        }
   
    }, function(e){
        logObject("registro", "loginError", e);
    });

});

var fecha_ano = $('#fecha_ano');

setSelectData($('#fecha_mes'), dateMonths);
setSelectData($('#fecha_dia'), dateDays);
setSelectData(fecha_ano, dateYears);

fecha_ano.val((new Date().getFullYear()-minDate));

if(Storage.getLoginFacebook() !== null){
    parseFacebook(Storage.getLoginFacebook());
    //dialogo.open();
    Storage.setLoginFacebook(null);
}

if(Storage.getLoginGoogle() !== null){
    parseGoogle(Storage.getLoginGoogle());
    //dialogo.open();
    Storage.setLoginGoogle(null);
}

$('.switch-box').each(function(){

    var self = $(this);
    var activeClass = self.data("active");
    var value = self.find(".switch-value");

    self.find(".switch-item").on("click", function(){

        var item = $(this);
        self.find(".switch-item").removeClass(activeClass);
        item.addClass(activeClass);
        value.val(item.data("val"));

    });

    self.find(".switch-item[data-val="+value.val()+"]").trigger("click");

});

/*
facebookConnectPlugin.getLoginStatus(function (data) {

    if(data.status == 'connected'){

        facebookConnectPlugin.logout(function(){
            app.toast.show({ text: "Facebook logout: on.", closeButton: true });

        }, function(){
            app.toast.show({ text: "Facebook logout: off.", closeButton: true });

        });

     }
     else {
         app.toast.show({ text: "Facebook off.", closeButton: true });
     }

});
*/