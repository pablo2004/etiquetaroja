var promocion = Storage.getParam("promocion");

var altaFavorito = function(id){

    ajax("GET", URL+"altaFavorito/"+Storage.getUser().id+"/"+id, {}, 
    function(){
        app.preloader.show();
    }, 
    function(data){
    
        app.preloader.hide();
    
        if(data['status'] == 1){
            favorito.data("fav", 1);
            favorito.addClass("fas");
            favorito.removeClass("far");
            app.toast.show({ text: "¡Promocion agregada a favoritos!", closeButton: true });
        }
    
    }, function(e){
        app.preloader.hide();
        app.toast.show({ text: e, closeButton: true });
    }, { "Token": Storage.getUser().oid });    

}

var borrarFavorito = function(id){

    ajax("GET", URL+"borrarFavorito/"+Storage.getUser().id+"/"+id, {}, 
    function(){
        app.preloader.show();
    }, 
    function(data){
    
        app.preloader.hide();
    
        if(data['status'] == 1){
            favorito.data("fav", 0);
            favorito.addClass("far");
            favorito.removeClass("fas");
            app.toast.show({ text: "¡Promocion eliminada de favoritos!", closeButton: true });
        }
    
    }, function(e){
        app.preloader.hide();
        app.toast.show({ text: e, closeButton: true });
    }, { "Token": Storage.getUser().oid });    

}

var esFavorito = function(id){

    ajax("GET", URL+"esFavorito/"+Storage.getUser().id+"/"+id, {}, 
    function(){}, 
    function(data){
    
        favorito.data("on", 1);

        if(data['status'] == 1){
            favorito.data("fav", 1);
            favorito.addClass("fas");
            favorito.addClass("fa-star");
        }
        else {
            favorito.data("fav", 0);
            favorito.addClass("far");
            favorito.addClass("fa-star");
        }
    
    }, function(e){}, 
    { "Token": Storage.getUser().oid });    

}

if(promocion !== null){

    var imagen = $('#imagen');
    var fecha = $('#fecha');
    var descripcion = $('#promoDescripcionCompleta');
    var terminos = $('#terminos');
    var promocionTitle = $('#promocionTitle');
    var favorito = $('#favorito');
    var promoObtener = $('#promo-obtener');

    if(Storage.getUser() !== null){
        esFavorito(promocion.id);
    }

    favorito.on("click", function(e){

        if(favorito.data("on") == 1){

            if(favorito.data("fav") == 1){
                borrarFavorito(promocion.id);
            }
            else{
                altaFavorito(promocion.id);
            }

        }

    });

    var terminosContenedor = $('#terminosContenedor');

    /////////////////////////////////////////////

    if(promocion.terminos.length !== 0){
        terminosContenedor.show();
        terminos.html(promocion.terminos);
    }
    else {
        terminosContenedor.hide();
    }

    promocionTitle.html(promocion.nombre);
    fecha.html("Valido hasta: "+promocion.fecha_termino)
    descripcion.html(promocion.descripcion_completa);
    imagen.html(promocion.nombre);
    imagen.css({"background-image": "url('"+promocion.imagen_promocion+"')"});

    promoObtener.data("params", objectToBase64(promocion));

}
else {
    mainView.router.back(url, {});
}