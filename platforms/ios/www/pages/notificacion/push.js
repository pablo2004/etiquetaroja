
var push = Storage.getPush();
Storage.setPush(null);

if(push !== null){

    var pushIr = $('#pushIr');
    var pushNombre = $('#pushNombre');
    var pushImagen = $('#pushImagen');
    var pushMensaje = $('#pushMensaje');
   
    pushNombre.html(push.nombre);
    pushMensaje.html(push.mensaje);

    if(push.imagen.length > 0){
        pushImagen.show();
        pushImagen.attr("src", push.imagen);
    }
    else {
        pushImagen.hide();
    }

    if(push.ruta.length == 0){
        pushIr.parent().hide();
    }
    else {
        pushIr.data("name", push.ruta);
        pushIr.data("params", push.params);
    }

}
else {
    mainView.router.back(url, {});
}