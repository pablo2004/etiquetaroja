
var accionEscanear = $('#accionEscanear');
var accionCanjear = $('#accionCanjear');
var codigoEscaner = $('#codigoEscaner');

var premioContenedor = $('#premio-contenedor');
var premioMensaje = $('#premio-mensaje');

var appElement = $('#app');

var prepareQr = function(){

    QRScanner.prepare(
        function onDone(err, status){
          if (err) {
           console.error(err);
          }
          if (status.authorized) {
          } else if (status.denied) {
          } else {
          }
    });

}

var escanearQrOff = function(button){

    QRScanner.hide(function(){});
    appElement.show();
    if(button){
        button.remove();
    }
    Storage.setScanning(false);

}

var escanearQr = function(){

    QRScanner.getStatus(function(status){

        if(status.authorized && status.canOpenSettings){

            Storage.setScanning(true);
    
            appElement.hide();
    
            var html = '';
            html += '<div class="fab fab-right-bottom color-orange">';
            html += '<a href="#" class=""><i class="fas fa-times"></i></a>';
            html += '</div>';
    
            var button = $(html);
    
            button.click(function(){
                escanearQrOff(button);
            });
    
            $('body').append(button);
    
            QRScanner.scan(function(err, contents){
                if(err){
                    codigoEscaner.val(err._message);
                    escanearQrOff(button);
                }
                else{
                    codigoEscaner.val(contents);
                    escanearQrOff(button);
                }
                
            });
    
            QRScanner.show(function(){});
    
        }
        else{
            alert("No tienes permiso para usar la camara.");
        }
    
    });

};


var initCanjearCodigo = function(id, codigo){

    data = {"codigo": codigo};

    ajax("POST", URL+"canjear/"+id, data, 
    function(){
        app.preloader.show();
    }, 
    function(data){
    
        app.preloader.hide();
    
        if(data['status'] == 0){
            app.toast.show({ text: data['message'], closeButton: true });
        }
        else{
            codigoEscaner.val("");
            premioMensaje.html(data['message']);
            premioContenedor.show();
        }
    
    }, function(e){
        app.preloader.hide();
        app.toast.show({ text: e, closeButton: true });
    }, { "Token": Storage.getUser().oid });    

}

accionEscanear.on("click", function(e){

    e.preventDefault();
    escanearQr();

});

accionCanjear.on("click", function(e){

    e.preventDefault();
    initCanjearCodigo( Storage.getUser().id, codigoEscaner.val());

});

prepareQr();