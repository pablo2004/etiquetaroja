// INIT CARUSE

var promocionCategoria = $('#promocionCategoria');

var initPorCategoria = function(categoria_id){

    ajax("POST", URL+"promociones", {"categoria_id": categoria_id}, 
    function(){
        app.preloader.show();
    }, 
    function(data){
    
        app.preloader.hide();
    
        if(data['status'] == 1){

            promocionCategoria.find('a').remove();
            var template = '';
            
            template += '<a data-name="promocion" data-params="{params}" class="go-link" href="/">';
            template += '<div class="card demo-card-header-pic">';
            template += '<div style="background-image:url({imagen})" class="card-header align-items-flex-end title-shadow">';
            template += '  {nombre}';
            template += '</div>';
            template += '<div class="card-content card-content-padding">';
            template += '  <p>{descripcion}</p>';
            template += '</div>';
            template += '</div>';
            template += '</a>';

            var ads = data.ads;
            var ad = null;
            var vars = {};

            for(var i in ads){
                ad = ads[i];
                vars = {
                    "params": objectToBase64(ad),
                    "imagen": ad['imagen_promocion'],
                    "nombre": ad['nombre'],
                    "descripcion": ad['descripcion']
                };
                promocionCategoria.append(parseTemplate(template, vars));
            }

        }
    
    }, function(e){
        app.preloader.hide();
        app.toast.show({ text: e, closeButton: true });
    });    

}

var categoria = Storage.getParam("categoria");
initPorCategoria(categoria.categ_id);