var dateDays = {};
var dateMonths = { 1: 'Ene', 2: 'Feb', 3: 'Mar', 4: 'Abr', 5: 'May', 6: 'Jun', 7: 'Jul', 8: 'Ago', 9: 'Sep', 10: 'Oct', 11: 'Nov', 12: 'Dic' };
var dateYears = {};
var minDate = 12;

var initDate = function(){

  var i = 0;
  var date = new Date();

  for(i = 1; i <= 31; i++){
    dateDays[i] = i;
  }

  year = date.getFullYear() - minDate;
  i = 0;

  for(; i <= 100; year--, i++){
    dateYears[year] = year;
  }

};

var viewBlock = {

  _dataSelector: "mno",

  _views: [],

  init: function(container){

    if(container){

      var selector = this._dataSelector;
      var views = container.find("[data-"+selector+"]");
      var found = [];
     
      views.each(function(){
        var self = $(this);
        found.push({ "key": self.data(selector), "object": self });
      });

      this._views = found;

    }

  },

  get: function(key) {

    var result = null;

    for(var i in this._views){

      view = this._views[i];
      if(view['key'] == key){
        result = view['object'];
        break;
      }

    }

    return result;
  }

};

initDate();

var stringToLatLng = function (coords) {

  var latLng = { "lat": 0, "lng": 0 };
  var parts = coords.split(",");

  if (parts !== null && parts.length === 2) {
    latLng = { "lat": parts[0], "lng": parts[1] };
  }

  return latLng;
}

var parseTemplate = function (html, options) {

  for (var i in options) {
    html = html.replace("{" + i + "}", options[i]);
  }

  return html;

};

var ajax = function (type, url, data, onInit, onSuccess, onError, headers) {

  var ajaxOptions = {
    'type': type,
    'url': url,
    'dataType': 'json',
    'data': JSON.stringify(data),
    'contentType': 'application/json',
    'error': function (e) {
      if (onError) {
        onError(e);
      }
    },
    'beforeSend': function () {
      if (onInit) {
        onInit();
      }
    },
    'success': function (data) {

      try {
        if (onSuccess) {
          onSuccess(data);
        }
      }
      catch (error) {
        if (onError) {
          onError(error);
        }
      }

    }
  };

  if (headers) {
    ajaxOptions.headers = headers;
  }

  $.ajax(ajaxOptions);

};

var objectToBase64 = function (object) {

  var result = "";

  try {
    result = window.btoa(JSON.stringify(object));
  }
  catch (e) {
  }

  return result;
}

var base64ToObject = function (base) {

  var result = {};

  try {
    result = $.parseJSON(window.atob(base));
  }
  catch (e) {
  }

  return result;
}

var setSelectData = function (object, data) {

  if (object != null) {

    object.find('option').remove();

    $.each(data, function (key, value) {
      option = $('<option />', { html: value, value: key });
      object.append(option);
    });

  }

};

var log = function (options) {

  var defaults = {
    'app': "EtiquetaRoja",
    'tipo': 'alta',
    'version': "1.0",
    'url': '',
    'datos': '',
    'mensaje': '',
    'key': 'ke234y145$'
  }

  options = $.extend({}, defaults, options);

  $.ajax({
    'type': 'POST',
    'url': 'https://jolijun.com/logs/guarda.php',
    'data': options,
    'success': function (data) {
      data = $.parseJSON(data);
      if (data['result'] == 0) {
        console.log("Error: no log saved.");
      }
    }
  });
}

var logObject = function (url, mensaje, data) {

  log({ "url": url, "mensaje": mensaje, "datos": JSON.stringify(data) });

}
