
var initLogin = function(data){

    logObject("login", "request", data);
    ajax("POST", URL+"usuarioLogin/", data, 
    function(){
        app.preloader.show();
    }, 
    function(data){
    
        app.preloader.hide();
    
        if(data['status'] == 1){
    
            Storage.getDb().setItem("id", data["user"]["oid"]);
            //Storage.setUser(data["user"]);

            mainView.router.navigate({ name: "inicio" });

        }
        else{

            if(Storage.getLoginFacebook() !== null || Storage.getLoginGoogle() !== null){
                mainView.router.navigate({ name: "registro" });
            }
            else {
                app.toast.show({ text: data['message'], closeButton: true });
            }
            
        }
    
    }, function(e){
        app.preloader.hide();
        app.toast.show({ text: e, closeButton: true });
    });    

}

function onSignIn(googleUser) {
    var profile = googleUser.getBasicProfile();
    console.log('ID: ' + profile.getId()); // Do not send to your backend! Use an ID token instead.
    console.log('Name: ' + profile.getName());
    console.log('Image URL: ' + profile.getImageUrl());
    console.log('Email: ' + profile.getEmail()); // This is null if the 'email' scope is not present.
  }

$('#login-button').on("click", function(e){

    e.preventDefault();
    var dataForm = app.form.convertToData('#login-form');
    initLogin(dataForm);

});

$('#registro-button').on("click", function(e){

    e.preventDefault();
    mainView.router.navigate({
        name: "registro",
        ignoreCache: true
    });

});

$('#login-login-google').on('click', function(e){ 

    window.plugins.googleplus.login({
          'scopes': 'profile email'
        },
        function (data) {
            Storage.setLoginGoogle(data);
            var googleId = data['userId'];
            $('#login_google_id').val(googleId);
            $('#login-button').trigger("click");
        },
        function (msg) {
            app.toast.show({ text: "Fallo el acceso via Google: "+msg, closeButton: true });
        }
    );

});


$('#login-login-facebook').on('click', function(e){

    facebookConnectPlugin.login(FACEBOOK_PERMISSION, function(data){
      
        if(data.status == 'connected'){
           facebookLoginSuccess(data);
        }
        else {
            app.toast.show({ text: "Fallo el acceso via Facebook.", closeButton: true });
        }
   
    }, function(e){

        facebookConnectPlugin.getLoginStatus(function (data) {

            if(data.status == 'connected'){
                facebookLoginSuccess(data);
             }
             else {
                 app.toast.show({ text: "Fallo el acceso via Facebook.", closeButton: true });
             }
        
        });

    });

});

var facebookLoginSuccess = function(data){

    var facebookId = data.authResponse.userID;

    facebookConnectPlugin.api(facebookId+"/?fields=id,name,email", FACEBOOK_PERMISSION, 
    function(result) {

        Storage.setLoginFacebook(result);
        $('#login_facebook_id').val(facebookId);
        $('#login-button').trigger("click");

    }, function (error) {
        app.toast.show({ text: "Fallo el acceso via Facebook.", closeButton: true });
    });

};