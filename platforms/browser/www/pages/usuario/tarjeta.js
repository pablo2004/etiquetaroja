 // INIT CARUSE
var tarjetas = $('#tarjetas');

var initTarjetas = function(id, token){

    ajax("GET", URL+"tarjetas/"+id, {}, 
    function(){
        app.preloader.show();
    }, 
    function(data){
    
        app.preloader.hide();
    
        if(data['status'] == 1){

            tarjetas.html("");
            tarjetas.find("a").remove();
            
            var template = '';
            
            /*
            template += '<a data-name="promocion" data-params="{params}" class="go-link">';
            template += '<div class="card">';
            template += '    <div style="height:120px;background-image:url({imagen});" class="card-header align-items-flex-end title-shadow"></div>';

            template += '    <div class="card-content">';

            template += '        <div class="card-box-rating">';
            template += '         <div class="card-box-score">{score}</div>';
            template += '         <div class="card-box-detail"><b>{nombre}</b><br />{descripcion}</div>';
            template += '         <div class="clearfix"></div>';
            template += '        </div>'

            template += '    </div>';
            template += '</div>';
            template += '</a>';
            */

           template += '<li data-name="tarjetadetalle" data-params="{params}" class="go-link">';
           template += '<a href="#" class="item-link item-content">';
           template += '<div class="item-media">';
           template += '<img src="{imagen}" width="44"/>';
           template += '</div>';
           template += '<div class="item-inner">';
           template += '<div class="item-title-row"><div class="item-title">{nombre} - {score}</div></div>';
           template += '<div class="item-subtitle">{descripcion}</div>';
           template += '</div>';
           template += '</div>';
           template += '</a>';
           template += '</li>';

            var cards = data.cards;
            var card = null;
            var vars = {};

            for(var i in cards){
                card = cards[i];
                vars = {
                    "params": objectToBase64(card),
                    "imagen": card['imagen_promocion'],
                    "nombre": card['nombre'],
                    "descripcion": card['descripcion'],
                    "score": card["canje"]["cuenta"]+"/"+card["acumulacion_requerida"]
                };
                tarjetas.append(parseTemplate(template, vars));
            }
    
        }
    
    }, function(e){
        app.preloader.hide();
        app.toast.show({ text: e, closeButton: true });
    },
    { "Token": token });    

}


initTarjetas(Storage.getUser().id, Storage.getUser().oid);