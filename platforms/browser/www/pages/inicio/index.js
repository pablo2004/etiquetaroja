// INIT CARUSE


var initPromociones = function(busqueda, lat, lng){

    var request = {
        "busqueda": busqueda,
        "lat": lat,
        "lng": lng
    };

    ajax("POST", URL+"promociones", request, 
    function(){
        app.preloader.show();
    }, 
    function(data){
    
        app.preloader.hide();
    
        if(data['status'] == 1){

            var promos = $('#promos');
            var template = '';

            template += '<a data-name="promocion" data-params="{params}" class="go-link" href="/">';
            template += '<div class="card">';
            template += '    <div class="card-content">';
            template += '        <div class="card-image-container">';
            template += '            <img class="card-image" src="{imagen}" />';
            template += '        </div>';
            
            template += '        <div style="padding: 10px; margin: 0px;">';
            template += '           <div style="width:{width};" class="card-description-image">';
            template += '               <div class="card-er-title">{nombre}</div>';
            template += '               <div class="card-er-description">{descripcion}</div>';
            template += '           </div>';
            template += '           <span style="display:{display};" class="card-image-circle">{porcentaje}</span>';
            //template += '           <p style="display:{display};" class="card-image-circle"><span class="card-image-percentaje">{porcentaje}</span></div>';
            template += '           <div class="clearfix"></div>';
            template += '        </div>';


            template += '    </div>';
            template += '</div>';
            template += '</a>';
            template += '<div class="clearfix"></div>';

            var ads = data.ads;
            var ad = null;
            var vars = {};

            for(var i in ads){
                ad = ads[i];

                if(ad['porcentaje'] == '0' || ad['porcentaje'] == ''){
                    display = 'none';
                    width = '100%';
                }
                else {
                    display = 'block';
                    width = '75%';
                }
                
                vars = {
                    "params": objectToBase64(ad),
                    "index": i,
                    "imagen": ad['imagen_promocion'],
                    "nombre": ad['nombre'],
                    "descripcion": ad['descripcion'],
                    "porcentaje": ad['porcentaje'],
                    "display": display,
                    "width": width
                };
                
                promos.append(parseTemplate(template, vars));
            }

            initCarusel();
    
        }
    
    }, function(e){
        app.preloader.hide();
        app.toast.show({ text: e, closeButton: true });
    });    

}

var initCarusel = function(){

    ajax("GET", URL+"carrusel", {}, 
    function(){
        app.preloader.show();
    }, 
    function(data){
    
        app.preloader.hide();
    
        if(data['status'] == 1){
    
            var carusel = $('#carusel');
            var wrapper = carusel.find('.swiper-wrapper');

            var template = '';
            template += '<div class="swiper-slide">';
            template += '<a href="#" data-params="{params}" data-name="promocion" class="go-link">';
            template += '  <img class="swiper-img" src="{imagen}" />';
            template += ' </a>';
            template += '</div>';
    
            var ads = data['ads'];
            var ad = null;
            var vars = null;
    
            for(var i in ads){
                ad = ads[i]
                vars = {
                    "params": objectToBase64(ad),
                    "imagen": ad['imagen_slide'],
                    "categoria": ad['categoria']
                };

                wrapper.append(parseTemplate(template, vars));
            }

            app.swiper.create('#carusel', {
                init: true,
                speed: 500,
                spaceBetween: 100,
                slidesPerView: 1,
                loop: true,
                centeredSlides: true,
                autoplay: {
                    delay: 5000,
                },
                pagination: {
                    el: '.swiper-pagination',
                    type: 'bullets',
                },
            });
    
            var id = Storage.getDb().getItem("id");

            if(id){
                initUser(id);
            }
            else {
                initMenu();
            }
            
        }
    
    }, function(e){
        app.preloader.hide();
        app.toast.show({ text: e, closeButton: true });
    });    

}

var initMenu = function(){

    ajax("GET", URL+"categorias", {}, 
    function(){}, 
    function(data){
    
        if(data['status'] == 1){

            var menu = $('#menu');
            var template = '';
            
            template += '<li data-name="categoria" data-sidebar="1" data-params="{params}" class="go-link">';
            template += '<div class="item-content">';
            template += '  <div class="item-media"><img src="{imagen}" width="30"/></div>';
            template += '  <div class="item-inner">';
            template += '    <div class="item-title-row">';
            template += '      <div class="item-title">{categoria}</div>';
            template += '    </div>';
            template += '  </div>';
            template += '</div>';
            template += '</li>';

            var categ = null;
            var vars = {};

            for(var i in data.categs){
                categ = data.categs[i];

                vars = {
                    "params": objectToBase64(categ),
                    "imagen": categ['imagen_categoria'],
                    "categoria": categ['categoria']
                };

                menu.append(parseTemplate(template, vars));
            }
    
        }
    

    }, function(e){});    

}

var initUser = function(oid){

    ajax("POST", URL+"usuarioLeer/", { "oid": oid }, 
    function(){
        app.preloader.show();
    }, 
    function(data){
    
        app.preloader.hide();
    
        if(data['status'] == 1){
    
            Storage.setUser(data["user"]);
            onLogged();

            if(typeof cordova != 'undefined'){
                cordova.plugins.firebase.messaging.getToken().then(function(token) {
                    saveUserData(Storage.getUser().id, "fcm_token", token);
                });
    
                navigator.geolocation.getCurrentPosition(function (position) {
                    var coords = position.coords.latitude.toFixed(7)+","+position.coords.longitude.toFixed(7);
                    saveUserData(Storage.getUser().id, "ultima_ubicacion", coords);
                }, function (error) {});
    
                cordova.plugins.firebase.messaging.subscribe("todo");
            }

            initMenu();

        }
    
    }, function(e){
        app.preloader.hide();
        app.toast.show({ text: e, closeButton: true });
    });    

}

var initBusqueda = function(busqueda){

    var searchList = $('#search-list');
    var promoBusqueda = $('#promo-busqueda');

    if(busqueda.length < 3){
        searchList.hide();
        promoBusqueda.find('li').remove();
    }
    else {

        ajax("POST", URL+"promociones", {"busqueda": busqueda}, 
        function(){
            app.preloader.show();
        }, 
        function(data){
        
            app.preloader.hide();
        
            if(data['status'] == 1){
    
                promoBusqueda.find('li').remove();
                
                var template = '';
            
                template += '<li data-i>';
                template += '<a data-name="promocion" data-params="{params}" href="#" class="go-link item-link item-content">';
                template += '  <div class="item-media"><img src="{imagen}" width="80"/></div>';
                template += '  <div class="item-inner">';
                template += '    <div class="item-subtitle">{nombre}</div>';
                template += '    <div class="item-text">{descripcion}</div>';
                template += '  </div>';
                template += '</a>';
                template += '</li>';
    
                var ads = data.ads;
                var ad = null;
                var vars = {};
    
                if(ads.length == 0){
                    searchList.hide();
                }
                else {
                    searchList.show();
                }
    
                for(var i in ads){
                    ad = ads[i];
                    vars = {
                        "params": objectToBase64(ad),
                        "imagen": ad['imagen_promocion'],
                        "nombre": ad['nombre'],
                        "descripcion": ad['descripcion']
                    };
                    promoBusqueda.append(parseTemplate(template, vars));
                }
    
            }
        
        }, function(e){
            app.preloader.hide();
            app.toast.show({ text: e, closeButton: true });
        });

    }    

}

var saveUserData = function(id, campo, valor){
    ajax("POST", URL+"usuarioGuarda/"+id+"/"+campo, { "valor": valor }, 
    function(){}, function(data){
    }, function(e){}, {"Token": Storage.getUser().oid });    
}

// INIT INDEX


if(Storage.getLat() !== 0 && Storage.getLat() !== 0){
    initPromociones("", Storage.getLat(), Storage.getLng());
}
else {
    
    app.preloader.show();
  
    navigator.geolocation.getCurrentPosition(function (position) {

        var lat = position.coords.latitude.toFixed(7);
        var lng = position.coords.longitude.toFixed(7);
    
        initPromociones("", lat, lng);
    
      }, function (error) {
        initPromociones("", 0, 0);
      });

}

$('#clave-busqueda').on("keyup", function(){

    initBusqueda($(this).val());

});

var onLogged = function(){
    
    var logoutLink = $('#logout-link');
    $('.show-onlogin').show();

    if(Storage.getUser() != null && Storage.getUser().sucursal_id != "0"){
        $('#qr-link').show();
    }

    logoutLink.on('click', function(e){
        e.preventDefault();
    
        app.dialog.create({
            title: 'Confirmar',
            text: '¿Deseas cerrar sesión?',
            buttons: [
              {
                text: 'Aceptar',
                onClick: function(){
                    Storage.getDb().clear();
                    Storage.setUser(null);

                    mainView.router.navigate(mainView.router.currentRoute.url, {
                        reloadCurrent: true,
                        ignoreCache: true,
                    });

                }
              },
              {
                text: 'Cancelar',
              }
            ]
          }).open();

          app.panel.get('.panel-left').close();
    
    });
}