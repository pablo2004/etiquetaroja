cordova.define('cordova/plugin_list', function(require, exports, module) {
module.exports = [
    {
        "file": "plugins/cordova-plugin-qrscanner/www/www.min.js",
        "id": "cordova-plugin-qrscanner.QRScanner",
        "pluginId": "cordova-plugin-qrscanner",
        "clobbers": [
            "QRScanner"
        ]
    },
    {
        "file": "plugins/cordova-plugin-qrscanner/src/browser/plugin.min.js",
        "id": "cordova-plugin-qrscanner.QRScannerProxy",
        "pluginId": "cordova-plugin-qrscanner",
        "runs": true
    },
    {
        "file": "plugins/cordova-plugin-googleplus/www/GooglePlus.js",
        "id": "cordova-plugin-googleplus.GooglePlus",
        "pluginId": "cordova-plugin-googleplus",
        "clobbers": [
            "window.plugins.googleplus"
        ]
    },
    {
        "file": "plugins/cordova-plugin-googleplus/src/browser/GooglePlusProxy.js",
        "id": "cordova-plugin-googleplus.GooglePlusProxy",
        "pluginId": "cordova-plugin-googleplus",
        "clobbers": [
            "GooglePlus"
        ]
    },
    {
        "file": "plugins/cordova-plugin-splashscreen/www/splashscreen.js",
        "id": "cordova-plugin-splashscreen.SplashScreen",
        "pluginId": "cordova-plugin-splashscreen",
        "clobbers": [
            "navigator.splashscreen"
        ]
    },
    {
        "file": "plugins/cordova-plugin-splashscreen/src/browser/SplashScreenProxy.js",
        "id": "cordova-plugin-splashscreen.SplashScreenProxy",
        "pluginId": "cordova-plugin-splashscreen",
        "runs": true
    },
    {
        "file": "plugins/cordova-plugin-key-hash/www/index.js",
        "id": "cordova-plugin-key-hash.getKeyHashes",
        "pluginId": "cordova-plugin-key-hash",
        "clobbers": [
            "window.plugins.getKeyHashes"
        ]
    },
    {
        "file": "plugins/cordova-plugin-key-hash/tests/index.spec.js",
        "id": "cordova-plugin-key-hash.tests",
        "pluginId": "cordova-plugin-key-hash"
    },
    {
        "file": "plugins/cordova-plugin-firebase-analytics/www/FirebaseAnalytics.js",
        "id": "cordova-plugin-firebase-analytics.FirebaseAnalytics",
        "pluginId": "cordova-plugin-firebase-analytics",
        "merges": [
            "cordova.plugins.firebase.analytics"
        ]
    },
    {
        "file": "plugins/cordova-plugin-firebase-messaging/www/FirebaseMessaging.js",
        "id": "cordova-plugin-firebase-messaging.FirebaseMessaging",
        "pluginId": "cordova-plugin-firebase-messaging",
        "merges": [
            "cordova.plugins.firebase.messaging"
        ]
    },
    {
        "file": "plugins/cordova-plugin-facebook4/www/facebook-browser.js",
        "id": "cordova-plugin-facebook4.FacebookConnectPluginBrowser",
        "pluginId": "cordova-plugin-facebook4",
        "clobbers": [
            "facebookConnectPlugin"
        ]
    }
];
module.exports.metadata = 
// TOP OF METADATA
{
    "cordova-plugin-whitelist": "1.3.4",
    "cordova-plugin-geolocation": "4.0.2",
    "cordova-support-google-services": "1.3.2",
    "cordova-plugin-qrscanner": "3.0.1",
    "cordova-plugin-enable-multidex": "0.2.0",
    "cordova-plugin-androidx": "1.0.2",
    "cordova-plugin-androidx-adapter": "1.1.0",
    "cordova-plugin-googleplus": "8.2.1",
    "cordova-plugin-splashscreen": "5.0.3",
    "cordova-plugin-key-hash": "0.1.0",
    "cordova-plugin-firebase-analytics": "4.3.0",
    "cordova-plugin-firebase-messaging": "4.4.0",
    "cordova-plugin-facebook4": "6.4.0"
}
// BOTTOM OF METADATA
});