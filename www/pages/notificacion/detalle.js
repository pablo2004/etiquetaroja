
var notificacion = Storage.getParam("notificacion");

if(notificacion !== null){

    var notificacionNombre = $('#notificacionNombre');
    var notificacionImagen = $('#notificacionImagen');
    var notificacionDescripcion = $('#notificacionDescripcion');
   
    notificacionNombre.html(notificacion.nombre);
    notificacionDescripcion.html(notificacion.descripcion);

    if(notificacion.imagen.length > 0){
        notificacionImagen.show();
        notificacionImagen.attr("src", notificacion.imagen);
    }
    else {
        notificacionImagen.hide();
    }

}
else {
    mainView.router.back(url, {});
}