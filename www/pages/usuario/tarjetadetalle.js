var tarjetadetalle = Storage.getParam("tarjetadetalle");

var getPromocion = function(){

    var tdContainer = viewBlock;
    tdContainer.init($('#tarjetadetalle'));

    areaScan = tdContainer.get("areaScan");
    verCodigoQR = tdContainer.get("verCodigoQR");
    verCodigoBC = tdContainer.get("verCodigoBC");

    promocionQr = tdContainer.get("promocionQr");
    promocionBarcode = tdContainer.get("promocionBarcode");
    promocionTexto = tdContainer.get("promocionTexto");
    promocionReportar = tdContainer.get("promocionReportar");
    promocionAcumulada = tdContainer.get("promocionAcumulada");

    promo = tarjetadetalle['canje'];
    sucursal = tarjetadetalle["sucursal"]

    cuenta_requerida = parseInt(tarjetadetalle['acumulacion_requerida']);
    cuenta = parseInt(tarjetadetalle['canje']['cuenta']);

    if(cuenta >= cuenta_requerida){
        promocionAcumulada.show();
        promocionAcumulada.html(tarjetadetalle['mensaje_acumulacion']);
    }
    else {
        promocionAcumulada.hide();
    }
    
    if(tarjetadetalle.promo_qr && tarjetadetalle.promo_barcode){

        areaScan.show();

        codigoQr = (tarjetadetalle.promo_texto.length === 0) ? tarjetadetalle["codigo"] : tarjetadetalle.promo_texto;
        
        barcode = 'https://jolijun.com/barcode/html/image.php?filetype=PNG&dpi=72&thickness=30&scale=2&rotation=0&font_family=Arial.ttf&font_size=8&setChecksum=&code=BCGcode39&text='+codigoQr;
        qrcode = 'https://api.qrserver.com/v1/create-qr-code/?size=150x150&data='+codigoQr;

      
        promocionQr.attr("src", qrcode);
        promocionBarcode.attr("src", barcode);

        promocionQr.hide();
        promocionBarcode.hide();

        verCodigoQR.on("click", function(e){
            verCodigoQR.addClass("button-active");
            verCodigoBC.removeClass("button-active");
            promocionQr.show();
            promocionBarcode.hide();
        });

        verCodigoBC.on("click", function(e){
            verCodigoQR.removeClass("button-active");
            verCodigoBC.addClass("button-active");
            promocionQr.hide();
            promocionBarcode.show();
        });

        verCodigoQR.trigger("click");
    
    }
    else{

        if(!tarjetadetalle.promo_qr && !tarjetadetalle.promo_barcode){
            areaScan.hide();
        }
        else {

            areaScan.show();
            areaScan.find('.segmented').hide();

            codigoQr = (tarjetadetalle.promo_texto.length === 0) ? promo["codigo"] : tarjetadetalle.promo_texto;
        
            barcode = 'https://jolijun.com/barcode/html/image.php?filetype=PNG&dpi=72&thickness=30&scale=2&rotation=0&font_family=Arial.ttf&font_size=8&setChecksum=&code=BCGcode39&text='+codigoQr;
            qrcode = 'https://api.qrserver.com/v1/create-qr-code/?size=150x150&data='+codigoQr;

         
            promocionBarcode.attr("src", barcode);

            if(tarjetadetalle.promo_qr){
                promocionQr.attr("src", qrcode);
                promocionQr.show();
                promocionBarcode.hide();
            }
            else {
                promocionBarcode.attr("src", qrcode);
                promocionBarcode.show();
                promocionQr.hide();
            }

        }

    }

    if(tarjetadetalle.promo_texto.length !== 0){
        promocionTexto.show();
        promocionTexto.find('.codigo-promocional').html(tarjetadetalle.promo_texto);
    }
    else{
        promocionTexto.hide();
    }

    promocionWhatsapp = tdContainer.get("promocionWhatsapp");
    contactoWhatsapp = tdContainer.get('contactoWhatsapp');

    if(tarjetadetalle.promo_whatsapp && sucursal){

        promocionWhatsapp.show();
        whatsapp = promocionWhatsapp.find('.share-whatsapp');
        contactoWhatsapp.html("Ponte en contacto con "+sucursal["nombre"]+" para hacer valida tu promoci&oacute;n.");

        whatsapp.on("click", function(e){
            e.preventDefault();

             w_texto = tarjetadetalle.promo_whatsapp_texto;
             var shareText = (w_texto !== null && w_texto.length > 0) ? w_texto : tarjetadetalle.nombre;

             window.location.href = "https://wa.me/"+sucursal["whatsapp"]+"?text="+shareText+" - "+APP_URL+"canjes/canjear/?codigo="+promo.codigo;
            
        });

    }
    else{
        promocionWhatsapp.hide();
    }

    promocionReportar.on("click", function(){

         var mensaje = '';

         mensaje += '<img style="width:100%;" src="img/er_escucha.png" />';
         mensaje += '<div style="text-align:center;font-size:10px;color:#A4A4A4;">envianos tus comentarios<br />pronto nos estaremos poniento en contacto contigo</div>';

        app.dialog.prompt("", mensaje, function (mensaje) {
            reportar(promocion.id, mensaje);
        });

    });

}

var reportar = function(id, mensaje){

    ajax("POST", URL+"reportar/"+Storage.getUser().id, {"promocion_id": id, "mensaje": mensaje}, 
    function(){
        app.preloader.show();
    }, 
    function(data){
    
        app.preloader.hide();
    
        if(data['status'] == 1){
    
            app.toast.show({ text: "¡Gracias por tu mensaje!", closeButton: true });

        }
    
    }, function(e){
        app.preloader.hide();
        app.toast.show({ text: e, closeButton: true });
    }, { "Token": Storage.getUser().oid });    

}

if(tarjetadetalle !== null){

    getPromocion();

}
else {
    mainView.router.back(url, {});
}