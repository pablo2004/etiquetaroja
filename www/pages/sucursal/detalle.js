var sucursal = Storage.getParam("sucursal");

if(typeof sucursal !== 'undefined'){

    var coords = stringToLatLng(sucursal.coordenadas);

    var mapa = new GMaps({
        div: '#mapaSucursal',
        lat: coords.lat,
        lng: coords.lng,
        zoom: 13
    });

    mapa.addMarker({
        lat: coords.lat,
        lng: coords.lng,
        title: sucursal.nombre,
        icon: "https://etiquetaroja.mx/cdn/store.png",
        infoWindow: {
            content: ""
        }
    });

    var sucursalImagen = $('#sucursalImagen');
    var sucursalDireccion = $('#sucursalDireccion');

    sucursalImagen.html(sucursal.nombre);
    sucursalImagen.css({"background-image": "url("+sucursal.imagen_cliente+")"});

    sucursalDireccion.html(sucursal.direccion);

}