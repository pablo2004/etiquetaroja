mapaContenedor = $('#mapaContenedor');
lista = $('#lista');

var mapa = null;
var latLng = { "lat": 25.6481239, "lng": -100.3341 };

navigator.geolocation.getCurrentPosition(function (position) {

    latLng = {"lat": position.coords.latitude.toFixed(7), "lng": position.coords.longitude.toFixed(7)};

    mapa = new GMaps({
        div: '#mapa',
        lat: latLng.lat,
        lng: latLng.lng,
        zoom: 13
    });

    getUbicacion(10, latLng);

}, function (error) {

    mapa = new GMaps({
        div: '#mapa',
        lat: latLng.lat,
        lng: latLng.lng,
        zoom: 13
    });

    getUbicacion(10, latLng);

});

var getUbicacion = function (distance, pos) {

    ajax("POST", URL + "ubicacion/" + distance, pos,
        function () {
            app.preloader.show();
        },
        function (data) {

            app.preloader.hide();

            if (data['status'] == 1) {

                mapa.removeMarkers();
                lista.find('ul').find('li').remove();

                var template = '';
                template += '<li class="go-link" data-name="promocion" data-params="{params}">';
                template += '<div class="item-content">';
                template += '<div class="item-media"><img src="{imagen}" width="44"/></div>';
                template += '<div class="item-inner">';
                template += '<div class="item-title-row">';
                template += '<div class="item-title">{nombre}</div>';
                template += '</div>';
                template += '<div class="item-subtitle">{descripcion}</div>';
                template += '</div>';
                template += '</div>';
                template += '</li>';

                mapa.addMarker({
                    lat: pos.lat,
                    lng: pos.lng,
                    title: "Yo",
                    icon: "https://etiquetaroja.mx/cdn/user.png"
                });

                var client = null, item = null;
                var coords = null;
                var infoWindow = "";
                var params = ""

                for (var a in data['clients']) {
                    item = data['clients'][a];
                    client = item.ad;

                    params = objectToBase64(client);
                    coords = stringToLatLng(item.place.coordenadas);
                    infoWindow = "";

                    infoWindow += '<a data-params="'+params+'" data-name="promocion" class="go-link" href="#">';
                    infoWindow += '<img src="'+client['imagen_promocion']+'" style="width:200px;" />';
                    infoWindow += '<br >'+client['nombre'];
                    infoWindow += '</a>';

                    mapa.addMarker({
                        lat: coords.lat,
                        lng: coords.lng,
                        title: client['nombre'],
                        infoWindow: {
                            content: infoWindow
                        }
                    });

                    vars = {
                        "params": params,
                        "nombre": client.nombre,
                        "descripcion": client.descripcion,
                        "imagen": client.imagen_cliente
                    };

                    lista.find("ul").append(parseTemplate(template, vars));    

                }

            }


        }, function (e) {
            app.preloader.hide();
            app.toast.show({ text: e, closeButton: true });
        });

}

var range = app.range.create({
    el: '.range-slider',
    min: 0,
    max: 50,
    label: true,
    scale: true,
    step: 5,
    value: 10,
    scaleSteps: 5,
    on: {
        change: function (data) {
            getUbicacion(data.value, latLng);
            $('#kmsDistance').html(data.value);
        }
    }
});

var verMapa, verLista;
var mapa, lista;

verMapa = $('#verMapa');
verLista = $('#verLista');

verMapa.on("click", function (e) {
    e.preventDefault();
    mapaContenedor.show();
    lista.hide();
});

verLista.on("click", function (e) {
    e.preventDefault();
    mapaContenedor.hide();
    lista.show();
});